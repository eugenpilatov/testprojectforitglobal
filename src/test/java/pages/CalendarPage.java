package pages;

import com.codeborne.selenide.ElementsCollection;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextBlock;
import ru.yandex.qatools.htmlelements.element.TextInput;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class CalendarPage {

    /******************************************************/
    /********************HTML-ELEMENTS*********************/
    /******************************************************/

    private TextBlock htmlDownloadCalendar = new TextBlock($(By.xpath("//div[@id='schedule-overlay']")));
    private Button htmlLeftMenuButton = new Button($(By.xpath("//a[@id='m_aside_left_minimize_toggle']")));
    private Button htmlReportsButtonInLeftMenu = new Button($(By.xpath("//a[contains (@class, 'menu__toggle')]//span[contains (text(), 'Отчеты')]")));
    private Button htmlReportForTodayButton = new Button($(By.xpath("//span[contains (text(), 'Отчет за сегодня')]")));


    /******************************************************/
    /************************METHODS***********************/
    /******************************************************/

    @Step("Ожидание загрузки календаря")
    public void waitDownloadCalendar() throws InterruptedException {
        $(htmlDownloadCalendar.getWrappedElement()).shouldHave(attribute("style", "display: none;"));
    }

    @Step("Переход на страницу отчета за сегодня")
    public void goToReportForTodayPage(){
        htmlLeftMenuButton.click();
        htmlReportsButtonInLeftMenu.click();
        htmlReportForTodayButton.click();
    }
}