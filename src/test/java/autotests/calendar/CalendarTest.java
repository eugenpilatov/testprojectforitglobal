package autotests.calendar;

import autotests.authorized.AuthorizedREST;
import configurations.ConfProperties;
import configurations.DataProviderClass;
import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.CalendarPage;
import pages.ReportForTodayPage;

import java.util.Map;

import static com.codeborne.selenide.Selenide.open;

public class CalendarTest extends AuthorizedREST {

    CalendarPage calendarPage;
    ReportForTodayPage reportForTodayPage;
    ConfProperties confProperties = ConfProperties.getConfProperties();

    @BeforeMethod
    @Step("Открытие календаря и переход на страницу с отчетом за сегодня")
    public void setupCalendar() throws InterruptedException {
        open(confProperties.getProperty("calendarPage"));
        calendarPage = new CalendarPage();
        calendarPage.goToReportForTodayPage();
    }

    @Test(dataProviderClass = DataProviderClass.class, dataProvider = "test-data")
    @Description ("Проверка возможности логирования времени")
    public void checkCalendarCurrentMonth(String mood) throws InterruptedException {
        reportForTodayPage = new ReportForTodayPage();
        reportForTodayPage.chooseMood(ReportForTodayPage.Mood.valueOf(mood));
        reportForTodayPage.submitReport();

        Assert.assertEquals(
                reportForTodayPage.getTextOfWarning(),
                "Вы хотите залогировать больше или меньше 0 часов, которые по графику запланированы у вас на сегодня",
                "Текст предупреждения не совпадает с ожидаемым"
        );
        reportForTodayPage.clickCancelButton();
    }
}